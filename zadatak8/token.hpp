#ifndef _TOKEN_
#define _TOKEN_
#include <string>
#include <iostream>

const int TAGBEGIN = 1;
const int TAGEND = 2;
const int TAGCLOSE = 3;
const int TAGENDANDCLOSE = 4;
const int ATTRIBUTENAME = 5;
const int EQUAL = 6;
const int ATTRIBUTEVALUE = 7;
const int CONTENT = 8;

std::string getTokenName(int tag);

class Token{
  public:
    int tag;
    std::string lexeme;
    int tagColumn = 0;
    int tagLine = 0;

    Token(int i=0, std::string const & s=""):tag(i), lexeme(s) {}
    Token& operator=(Token const & t){
      tag = t.tag;
      lexeme = t.lexeme;
      return *this;
    }
    void print(){
      std::cout << "<" << getTokenName(tag) << ", " << lexeme << ">: " <<"line "<<tagLine<<" ,column "<<tagColumn<< std::endl;
    }

};

std::string getTokenName(int tag){
  switch(tag){
    case TAGBEGIN:
      return "TAGBEGIN";
    case TAGEND:
      return "TAGEND";
    case TAGCLOSE:
      return "TAGCLOSE";
    case TAGENDANDCLOSE:
      return "TAGENDANDCLOSE";
    case ATTRIBUTENAME:
      return "ATTRIBUTENAME";
    case EQUAL:
      return "EQUAL";
    case ATTRIBUTEVALUE:
      return "ATTRIBUTEVALUE";
    case CONTENT:
      return "CONTENT";
    case 0:
      return "unknown";
    default:
      return std::string(1, tag)  ;
  }
}

#endif
