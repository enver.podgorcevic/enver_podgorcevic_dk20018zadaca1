%{

#include "token.hpp"
int nTagBegin = 0;
int nTagEnd = 0;
int nTagClose = 0;
int nTagAttName = 0;
int nEqual = 0;
int nTagAttVal = 0;
int nTagCon = 0;
int nTagEndClose = 0;
int line = 1;
int column = 1;

%}

ws            [ \t\n]
letter        [a-zA-Z]
digit         [0-9]
number        {digit}+
identifier    {letter}({letter}|{digit})*
special	      [?\-;.!$#]

word          {letter}+
value         \"{identifier}\"
tag_begin     \<{word}
tag_end       [\>]
tag_close     \<\/{word}\>
content       {word}({word}|[ \t])*
new_line      \n
tag_end_close \/\>

%%

{tag_begin}          { nTagBegin++; return TAGBEGIN; }
{tag_end}            { nTagEnd++; return TAGEND; }
{tag_close}          { nTagClose++; return TAGCLOSE; } 
{identifier}         { nTagAttName++; return ATTRIBUTENAME; }
"="                  { nEqual++; return EQUAL;}
{value}              { nTagAttVal++; return ATTRIBUTEVALUE;}
{content}            { nTagCon++; return CONTENT;}
{tag_end_close}      { nTagEndClose++; return TAGENDANDCLOSE; }
{new_line}           { line++; column=1; }
{ws}

%%

int main(void){
  int tok;
  int col = 0;
  while(tok = yylex() ){
    if(tok==ATTRIBUTEVALUE) {
    Token t(tok, std::string(yytext,1,sizeof(yytext)-2));
    t.tagLine = line;
    t.tagColumn = column;
    t.print();
    column += (t.lexeme).size();

    }
    else{
    Token t(tok, yytext);
    t.tagLine = line;
    t.tagColumn = column;
    t.print();
    column += (t.lexeme).size();
    }
  } 
  std::cout<<std::endl<<std::endl;
  std::cout<<"TAGBEGIN: "<<nTagBegin<<std::endl;
  std::cout<<"TAGEND: "<<nTagEnd<<std::endl;
  std::cout<<"TAGCLOSE: "<<nTagClose<<std::endl;
  std::cout<<"TAGENDANDCLOSE: "<<nTagEndClose<<std::endl;
  std::cout<<"ATTRIBUTENAME: "<<nTagAttName<<std::endl;
  std::cout<<"EQUAL: "<<nEqual<<std::endl;
  std::cout<<"ATTRIBUTEVALUE: "<<nTagAttVal<<std::endl;
  std::cout<<"CONTENT: "<<nTagCon<<std::endl;


  return 0;
}

int yywrap(){
  return 1;
}
