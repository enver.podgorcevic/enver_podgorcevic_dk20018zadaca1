#include "token.h"
#include "functions.h"

Token::Token(int tag, std::string lexeme) :
    _tag{tag}, _lexeme{lexeme} {}

Token& Token::operator=(Token const& token){
    _tag = token._tag;
    _lexeme = token._lexeme;
    return *this;
}

void Token::print() const{
    std::cout   << "<" << getTokenName(_tag) << ", "
                << _lexeme << ">: "
                << "line " << _line << ", "
                << "column " << _column << "\n"; 
}
