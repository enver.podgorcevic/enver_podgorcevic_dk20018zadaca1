#ifndef _TOKEN_
#define _TOKEN_

#include <string>
#include <iostream>

#define TAGBEGIN 1
#define TAGEND 2
#define TAGCLOSE 3
#define TAGENDANDCLOSE 4
#define ATTRIBUTENAME 5
#define EQUAL 6
#define ATTRIBUTEVALUE 7
#define CONTENT 8
#define WHITESPACE 9

class Token{
public:
    int _tag;
    std::string _lexeme;
    int _line = 0;
    int _column = 0;

    Token(int i = 0, std::string = "");
    Token& operator=(Token const& t);
    void print() const;
};

#endif
