#include <string>
#include "functions.h"
#include "token.h"
#include "inputbuffer.h"

std::string getTokenName(int tag){
    switch(tag){
        case TAGBEGIN:
            return "TAGBEGIN";
        case TAGEND:
            return "TAGEND";
        case TAGCLOSE:
            return "TAGCLOSE";
        case TAGENDANDCLOSE:
            return "TAGENDANDCLOSE";
        case ATTRIBUTENAME:
            return "ATTRIBUTENAME";
        case EQUAL:
            return "EQUAL";
        case ATTRIBUTEVALUE:
            return "ATTRIBUTEVALUE";
        case CONTENT:
            return "CONTENT";
        case 0:
            return "unknown";
        default:
            return std::string(1, tag);
    }
}

Token getWS(inputBuffer & b){
    b.startScan();
    char c = b.getNext();
    if( c != ' ' && c != '\t' && c != '\n' )
        return wrongToken(b);

    while( (c = b.getNext()) && ( c == ' ' || c == '\t' || c == '\n' ));

    if(c) b.backtrack(1);
    return Token(WHITESPACE);
}

Token getEqual(inputBuffer& b){
    b.startScan();
    char c ;
    c = b.getNext();
    if(c == '=') 
    {
        c = b.getNext();
        if(c == '"')
        {
            if(c) b.backtrack(1);
            return Token(EQUAL, b.getLexeme());
        }
        else 
            return wrongToken(b);
    }
    return wrongToken(b);
}

void skipWs(inputBuffer &b){
    b.startScan();
    char c ;
    while( (c = b.getNext()) && ( c == ' ' || c == '\t' || c == '\n' ));
    if(c) b.backtrack(1);
}


Token getTagBegin(inputBuffer& b){
    b.startScan();
    char c = b.getNext();
    if(c == '<'){
        c = b.peekNext();
        if(!isalpha(c))
            return wrongToken(b);
        while( (c = b.getNext())&& isalpha(c) );
        if(c) b.backtrack(1);
        return Token(TAGBEGIN, b.getLexeme());
    }
    else
        return wrongToken(b);
}

Token getAttributeName(inputBuffer& b){
    b.startScan();
    char c = b.getNext();
    if( isalpha(c) ){
        while((c = b.getNext()) && ( isalpha(c)||isalnum(c)));
        if(c == '=')
        {
            b.backtrack(1);
            return Token(ATTRIBUTENAME, b.getLexeme());
        }
    }
    return wrongToken(b);
}

Token getAttributeValue(inputBuffer& b){
    b.startScan();
    char c = b.getNext();
    if(c == '"'){
        c = b.getNext();
        while( (c = b.getNext()) && (isalpha(c) || isalnum(c)));
        if(c == '"' )
        {
            Token t = Token(ATTRIBUTEVALUE, b.getLexeme());
            int t_lenght = (t._lexeme).size();
            t._lexeme = std::string(t._lexeme,1 , t_lenght-2 );
            return t;
        }
    }
    return wrongToken(b);
}

Token getContent(inputBuffer& b){
    b.startScan();
    char c = b.getNext();
    if(isalpha(c)){

        while(isalpha(c) || (c == ' ')){
            c = b.getNext();
        }
        return Token(CONTENT,b.getLexeme());
    }
    return wrongToken(b);
}

Token getTagEnd(inputBuffer& b){
    b.startScan();
    char c = b.getNext();
    if(c == '>'){
        return Token(TAGEND, b.getLexeme());
    }
    else
        return wrongToken(b);
} 

Token getTagClose(inputBuffer& b){
    b.startScan();
    char c = b.getNext();
    if(c == '<')
    {
        c = b.getNext();
        if(c == '/' )
        { 
            c = b.getNext();
            if(isalpha(c))
            {
                while((c = b.getNext())&& isalpha(c) );
                if(c == '>')
                    return Token( TAGCLOSE, b.getLexeme());
            }
        }
    }
    return wrongToken(b);
}

Token wrongToken(inputBuffer & b){
    b.backtrack();
    return Token(0);
}
