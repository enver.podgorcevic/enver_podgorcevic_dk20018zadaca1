#ifndef _FUNCTIONS_
#define _FUNCTIONS_

#include <string>
#include "inputbuffer.h"
#include "token.h"

std::string getTokenName(int);

Token getWS(inputBuffer& buffer);
Token getTagBegin(inputBuffer& buffer);
Token getTagEnd(inputBuffer& buffer);
Token getTagClose(inputBuffer& buffer);
Token getTagiEndAndClose(inputBuffer& buffer);
Token getEqual(inputBuffer& buffer);
Token getAttributeName(inputBuffer& buffer);
Token getAttributeValue(inputBuffer& buffer);
Token getContent(inputBuffer& buffer);
Token wrongToken(inputBuffer& buffer);

#endif
