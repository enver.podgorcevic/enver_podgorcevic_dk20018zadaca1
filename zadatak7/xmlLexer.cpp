#include <iostream> 
#include <string> 
#include "xmlLexer.h" 

static int nTagBegin = 0, nTagEnd = 0, nTagClose = 0, nTagEndAndClose = 0, nAttributeName = 0, nEqual = 0, nAttributeValue = 0, nContent = 0;


int main() 
{ 
    std::string line; 
    int linnum=0; 
    while (std::getline(std::cin, line)){ 
        inputBuffer buffer(line); 
        while(!buffer.isEmpty()) { 
            Token t;
            if(0 != (t=getTagBegin(buffer))._tag ) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nTagBegin++;
            }
            else if(0 != (t=getEqual(buffer))._tag) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nEqual++;
            }
            else if(0 != (t=getAttributeValue(buffer))._tag ) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nAttributeValue++;
            }
            else if(0 != (t=getContent(buffer))._tag ) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nContent++;
            }

            else if(0 != (t=getTagEnd(buffer))._tag) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nTagEnd++;
            }
            else if(0 != (t=getTagClose(buffer))._tag) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nTagClose++;
            }
            else if(0 != (t=getAttributeName(buffer))._tag) {
                t._column = buffer._lexemeBegin+1; 
                t._line = linnum+1;
                nAttributeName++;
            }

            else if(0 != (t=getWS(buffer))._tag);
            else { 
                int ind = buffer._currentIndex; 
                std::cout << ">> Greska na mjestu ["<<  linnum << ":" << ind 
                    << "], na pocetku teksta '" 
                    << buffer.getRest() << "'." << std::endl ; 
            }
            if(t._tag != 0){
                if(t._tag != WHITESPACE)
                    t.print();
            }
        }   
        ++linnum; 
    } 
    std::cout << std::endl<<std::endl;
    std::cout<<"TAGBEGIN: "<<nTagBegin<<std::endl;
    std::cout<<"TAGEND: "<<nTagEnd<<std::endl;
    std::cout<<"TAGCLOSE: "<<nTagClose<<std::endl;
    std::cout<<"TAGENDANDCLOSE: "<<nTagEndAndClose<<std::endl;
    std::cout<<"ATTRIBUTENAME: "<<nAttributeName<<std::endl;
    std::cout<<"EQUAL:"<<nEqual<<std::endl;
    std::cout<<"ATTRIBUTEVALUE: "<<nAttributeValue<<std::endl;
    std::cout<<"CONTENT: "<<nContent<<std::endl;



    return 0; 
} 
