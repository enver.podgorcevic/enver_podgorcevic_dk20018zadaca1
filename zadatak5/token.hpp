#ifndef _TOKEN_
#define _TOKEN_
#include <string>
#include <iostream>

const int ID = 1;
const int OPENING_BRACKET = 2;
const int CLOSING_BRACKET = 3;
const int MULTIPLY = 4;
const int DIVIDE = 5;
const int PLUS = 6;
const int MINUS = 7;
const int ASSIGN = 8;
const int NUMBER = 9;

class Token{
  public:
    int tag;
    std::string lexeme;

    Token(int i=0, std::string const & s=""):tag(i), lexeme(s) {}
    Token& operator=(Token const & t){
      tag = t.tag;
      lexeme = t.lexeme;
      return *this;
    }
    void print(){
      std::cout << "<" << tag << ", " << lexeme << ">" << std::endl;
    }

};

#endif

