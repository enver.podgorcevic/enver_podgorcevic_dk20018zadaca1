#ifndef _FUNCTIONS_
#define _FUNCTIONS_

#include <string>
#include "inputbuffer.h"
#include "token.hpp"

static inputBuffer buffer;
void setBuffer(inputBuffer& b);
Token getID(inputBuffer& b);
Token getNumber(inputBuffer& b);
Token getBracket(inputBuffer& b);
Token getPlus(inputBuffer& b);
Token getMinus(inputBuffer& b);
Token getMultiply(inputBuffer& b);
Token getDivide(inputBuffer& b);
Token getAssign(inputBuffer& b);
Token getToken();

#endif
