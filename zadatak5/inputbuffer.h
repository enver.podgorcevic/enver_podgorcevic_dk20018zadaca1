#ifndef _INPUTBUFFER_
#define _INPUTBUFFER_

#include <string>
#include <iostream>

class inputBuffer{
    public:
        std::string _text;
        int _lexemeBegin = 0;
        int _currentIndex = 0;
        int _bufferLength = 0;
        int _previousIndex = 0;

        inputBuffer(std::string text = "");

        inputBuffer& operator=(std::string const & text);
        bool isEmpty() const;
        char peekNext() const;
        char getNext();
        std::string getLexeme() const;
        std::string getRest();
        void startScan();
        void retreat();
        void retract(int = 1);
        void backtrack(int = 0);
        void print() const;
};
#endif
