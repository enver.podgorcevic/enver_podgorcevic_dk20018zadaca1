#include "calculatorLexer.h"

int main(void){
    Token t;
    std::ifstream inFile;
    inFile.open("zadaca1.in");
    if (!inFile) {
        std::cerr << "Unable to open file zadaca1.in";
        exit(1);
    }
    std::string line;
    while (inFile >> line){
        inputBuffer b(line);
        setBuffer(b);
        while((t = getToken()).tag != 0){
            t.print();
        }
    }
    inFile.close();
    return 0;
}

