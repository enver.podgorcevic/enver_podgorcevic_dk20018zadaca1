#include "functions.h"

void setBuffer(inputBuffer& b){
    buffer=b;
}

Token getID(inputBuffer& b){
    char c;
    Token t;

    while(b.peekNext() && !isalpha(b.peekNext())){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty() || !isalpha(b.getNext())){
        return Token(0,"No more ID's");
    }
    else{
        while(b.peekNext() && isalpha(b.peekNext())){
            b.getNext();
        }
        t=Token(ID,b.getLexeme());
    }
    return t;
}

Token getNumber(inputBuffer&b){
    Token t;
    std::string s;
    char c;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }
    b.startScan();

    if(b.isEmpty() || !isdigit(b.peekNext())){
        return Token(0,"No More Tokens");   
    }

    else{
        while(b.peekNext() && isdigit(b.peekNext())){
            b.getNext();
        }
        t = Token(NUMBER, b.getLexeme());
    }
    return t;
}

Token getBracket(inputBuffer& b){
    Token t;
    std::string s;
    char c;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty()){
        return Token(0,"No more Tokens");   
    }
    else{
        while(b.peekNext()){
            if(b.peekNext() == '('){
                tag = OPENING_BRACKET;
                b.getNext();
                s = b.getLexeme();                                
                break;  
            }       
            else if(b.peekNext() == ')'){
                tag = CLOSING_BRACKET;
                b.getNext();
                s=b.getLexeme();                
                break;  
            }
            else{
                tag = 101;
                s = "Not bracket!";
                break;
            }
        }
        t=Token(tag, s); 
    }
    return t;
}

Token getPlus(inputBuffer& b){
    Token t;
    std::string s;
    char c;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty()){
        return Token(0,"No Tokens");    
    }
    else{
        while(b.peekNext()){
            if(b.peekNext() == '+'){
                tag = PLUS;
                b.getNext();
                s = b.getLexeme();
                break;
            }
            else{
                tag = 101;
                s = "Not plus!";
                break;
            }
        }
        t = Token(tag, s); 
    }
    return t;   
}

Token getMinus(inputBuffer& b){
    Token t;
    std::string s;
    char c;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty()){
        return Token(0,"No Tokens");
    }
    else{
        while(b.peekNext()){
            if(b.peekNext() == '-'){
                tag = MINUS;
                b.getNext();
                s = b.getLexeme();
                break;
            }
            else{
                tag = 101;
                s = "Not minus!";
                break;
            }
        }
        t=Token(tag, s); 
    }
    return t;   
}

Token getMultiply(inputBuffer& b){
    Token t;
    std::string s;
    char c;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty()){
        return Token(0,"No Tokens");    
    }
    else{
        while(b.peekNext()){
            if(b.peekNext() == '*'){
                tag = MULTIPLY;
                b.getNext();
                s = b.getLexeme();
                break;
            }
            else{
                tag = 101;
                s = "Not multiply!";
                break;
            }
        }
        t=Token(tag, s); 
    }
    return t;
}

Token getDivide(inputBuffer& b){
    char c;
    Token t;
    std::string s;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty()){
        return Token(0, "No Tokens");
    }
    else{
        while(b.peekNext()){
            if(b.peekNext() == '/'){
                tag = DIVIDE;
                b.getNext();
                s = b.getLexeme();
                break;
            }
            else{
                tag = 101;
                s = "Not divide!";
                break;
            }
        }
        t=Token(tag, s);
    }
    return t;
}

Token getAssign(inputBuffer& b){
    Token t;
    std::string s;
    char c;
    int tag;

    while(b.peekNext() && b.peekNext() == ' '){
        b.getNext();
    }

    b.startScan();
    if(b.isEmpty()){
        return Token(0,"No Tokens");    
    }
    else{
        while(b.peekNext()){
            if(b.peekNext() == '='){
                tag = ASSIGN;
                b.getNext();
                s = b.getLexeme();
                break;
            }
            else{
                tag = 101;
                s = "Not assign!";
                break;
            }
        }
        t=Token(tag, s); 
    }
    return t;
}

Token getToken(){
    char c;
    Token t;

    while(c=buffer.peekNext()){ 

        if(isalpha(c)){ 
            return getID(buffer);

        }
        else if(isdigit(c)){    
            return getNumber(buffer);

        }
        else if(c == '(' || c == ')'){    
            return getBracket(buffer);

        }
        else if(c == '+'){
            return getPlus(buffer);

        }
        else if(c == '-'){
            return getMinus(buffer);

        }
        else if(c == '*'){
            return getMultiply(buffer);

        }
        else if(c == '/'){
            return getDivide(buffer);

        }
        else if(c == '='){
            return getAssign(buffer);

        }
        else{
            buffer.getNext();

        }
    }
    return Token(0,"Not Token");
}
