#include "inputbuffer.h"

inputBuffer::inputBuffer(std::string s) : _text(s), _bufferLength(s.size()) {}

inputBuffer& inputBuffer::operator=(std::string const & text){
    _text = text;
    _lexemeBegin = _currentIndex = 0;
    _bufferLength = text.size();
    return *this;
}

bool inputBuffer::isEmpty() const{
    return(_currentIndex == _bufferLength) ;
}

char inputBuffer::peekNext() const{
    if(!isEmpty())
        return _text.at(_currentIndex);
    else
        return 0;
}

char inputBuffer::getNext(){
    if(!isEmpty()) {
        return _text.at(_currentIndex++);
    }
    else
        return 0;
}

std::string inputBuffer::getLexeme() const {
    return std::string(_text, _lexemeBegin, _currentIndex - _lexemeBegin);
}

std::string inputBuffer::getRest() {
    std::string rest(_text, _currentIndex);
    _currentIndex = _bufferLength;
    return rest;
}

void inputBuffer::startScan(){
    _lexemeBegin = _currentIndex;
}

void inputBuffer::retreat(){
    _currentIndex = _lexemeBegin;
}

void inputBuffer::retract(int n) {
    if(_currentIndex >= n)
        _currentIndex -= n;
}

void inputBuffer::backtrack(int n){
    if(n == 0)
        _currentIndex = _lexemeBegin;
    else
        if(_currentIndex >= n)
            _currentIndex -= n;
}

void inputBuffer::print() const{
  std::cout << "text: " << std::string(_text, _currentIndex) << std::endl;
  std::cout << "bufferLength: " << _bufferLength << std::endl;
  std::cout << "currentIndex: " << _currentIndex << std::endl;
  std::cout << "lexemeBegin: " << _lexemeBegin << std::endl;
  std::cout << "peekNext: " << peekNext() << std::endl;
}

