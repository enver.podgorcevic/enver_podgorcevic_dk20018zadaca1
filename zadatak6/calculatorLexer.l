%{
#include "token.hpp"
%}

%%

[a-zA-Z][a-zA-Z0-9_]*           {return ID;}
[0-9]+                          {return NUMBER;}
\(                              {return OPENING_BRACKET;}
\)                              {return CLOSING_BRACKET;}
\+                              {return PLUS;}
\-                              {return MINUS;}
\*                              {return MULTIPLY;}
\/                              {return DIVIDE;}
\=                              {return ASSIGN;}
[ \n\t]+                        {}

%%

int main(){
    int tag;
    while(tag = yylex()){
        Token t = Token(tag, yytext);
        if(t.tag != 0)
            t.print();
    }
    return 0;
}
